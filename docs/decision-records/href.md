# href

## Decision

* Use relative URLs
* End path with slash **when** it serves as a directory (e.g.: list resources)

## Context

Cons of absolute URLs:

http://example.com

http might change to https

//example.com

The domain might change

/subpath

The website might be deployed in a different subpath, rendering "Root-relative URL" invalid.
Faced some of these issues when trying to deploy services on subpaths on my home network,
ultimately deciding that using subdomains was easier.

Base-relative URL (home-page-relative) - `<base>`.
Renders links to anchors (in the same page) useless, e.g.: `href="#context` would actually link
to that anchor in my "home page".

## References

[html - Absolute vs relative URLs - Stack Overflow](https://stackoverflow.com/a/39545949)
[To slash or not to slash  |  Google Search Central Blog  |  Google for Developers](https://developers.google.com/search/blog/2010/04/to-slash-or-not-to-slash)
