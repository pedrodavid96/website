SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

BUILD_DIR := build/public
STATIC_SRC_DIR := src/public
STATIC_SRC_FILES := $(shell fd . $(STATIC_SRC_DIR) --type f)
STATIC_BUILD_FILES := $(STATIC_SRC_FILES:$(STATIC_SRC_DIR)/%=$(BUILD_DIR)/%)

# Default - top level rule is what gets ran when you run just `make`
.PHONY: build
build: build-sass build-static

.PHONY: build-sass
build-sass: $(BUILD_DIR)/styles/style.css

# Implicitly knows that src/sass/style.css includes and compiles all the other sass resources.
# Re-compile it any time one of those included sass files is changed.
$(BUILD_DIR)/styles/%.css: src/sass/%.sass $(wildcard src/sass/**/*.sass)
> sass --style=compressed --no-source-map $< $@

.PHONY: build-static
build-static: $(STATIC_BUILD_FILES)

$(BUILD_DIR)/%: $(STATIC_SRC_DIR)/%
  # Unfortunately it's impossible to suppress a single line with ONESHELL
  # So we suppress all and explicitly echo what we want
> @mkdir -p $(@D)
> echo "cp $< $@"
> cp $< $@

.PHONY: clean
clean:
> rm -rf build/

.PHONY: watch
watch: clean build
> live-server $(BUILD_DIR) &
  # xargs used to ignore argument piped from fswatch as suggested in docs
  # https://github.com/emcrisostomo/fswatch?tab=readme-ov-file#usage
> fswatch -o src | xargs -n1 -I{} $(MAKE) --no-print-directory
